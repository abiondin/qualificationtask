[TLU_0]
orientation = 0,0,0
orientation_mode = "xyz"
position = 0,0,0
role = "auxiliary"
time_resolution = 1s
type = "tlu"

[MIMOSA26_0]
mask_file = "mask_MIMOSA26_0.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -2.94225deg,1.66049deg,-0.0484722deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 919.473um,-290.217um,0
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_1]
mask_file = "mask_MIMOSA26_1.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 2.22502deg,-1.60325deg,-0.0378152deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 972.774um,-9.262um,151mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_2]
mask_file = "mask_MIMOSA26_2.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 1.27919deg,-1.55982deg,0.0512224deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 1.03242mm,-202.934um,304mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_3]
mask_file = "mask_MIMOSA26_3.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0,0,0
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 0,0,450mm
role = "reference"
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_4]
mask_file = "mask_MIMOSA26_4.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -1.9641deg,-1.00938deg,-0.390471deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 26.689um,-74.002um,603mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[USBPIXI4_10]
material_budget = 0.00075
number_of_pixels = 336, 80
orientation = 0,0,0
orientation_mode = "xyz"
pixel_pitch = 50um,250um
position = -2.68704mm,88.361um,696mm
role = "dut"
spatial_resolution = 14.4um,72.2um
time_resolution = 230us
type = "usbpixi4"

[MIMOSA26_5]
mask_file = "mask_MIMOSA26_5.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -2.89854deg,-1.51673deg,0.0257831deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 31.142um,-37.312um,753mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

