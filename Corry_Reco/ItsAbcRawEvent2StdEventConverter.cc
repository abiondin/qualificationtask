

#include "eudaq/StdEventConverter.hh"
#include "eudaq/RawEvent.hh"
#include <iostream>

#include <regex>

class ItsAbcRawEvent2StdEventConverter: public eudaq::StdEventConverter{
public:
  bool Converting(eudaq::EventSPC d1, eudaq::StdEventSP d2, eudaq::ConfigSPC conf) const override;
  static const uint32_t m_id_factory = eudaq::cstr2hash("ITS_ABC");
  static const uint32_t PLANE_ID_OFFSET_ABC = 10;
};

namespace{
  auto dummy0 = eudaq::Factory<eudaq::StdEventConverter>::
    Register<ItsAbcRawEvent2StdEventConverter>(ItsAbcRawEvent2StdEventConverter::m_id_factory);
}

typedef std::map<uint32_t, std::pair<uint32_t, uint32_t>> BlockMap;
// Normally, will be exactly the same every event, so remember
static std::map<uint32_t, std::unique_ptr<BlockMap>> map_registry;

bool ItsAbcRawEvent2StdEventConverter::Converting(eudaq::EventSPC d1, eudaq::StdEventSP d2,
						  eudaq::ConfigSPC conf) const{
  auto raw = std::dynamic_pointer_cast<const eudaq::RawEvent>(d1);
  if (!raw){
    EUDAQ_THROW("dynamic_cast error: from eudaq::Event to eudaq::RawEvent");
  }  

  std::string block_dsp = raw->GetTag("ABC_EVENT", "");
  if(block_dsp.empty()){
    return true;
  }

  auto block_n_list = raw->GetBlockNumList();
  //std::cout << block_n_list.size() << std::endl;	
  for(auto &block_n: block_n_list){
	if (block_n != 5) continue;
	//std:: cout << block_n <<" ";
	

    //auto it = block_map.find(block_n);
    //if(it == block_map.end())
    // continue;
    //if(it->second.second<3){
    //uint32_t strN = block_n->second.first;
    //uint32_t bcN = block_n->second.second;
      //uint32_t plane_id = PLANE_ID_OFFSET_ABC + bcN*10 + strN; 
      uint32_t plane_id = PLANE_ID_OFFSET_ABC + block_n; 
      std::vector<uint8_t> block = raw->GetBlock(block_n);
      std::vector<bool> channels;
      eudaq::uchar2bool(block.data(), block.data() + block.size(), channels);
      eudaq::StandardPlane plane(plane_id, "ITS_ABC", "ITS_ABC");
      d2->SetDetectorType("ITk_strip");
      plane.SetSizeZS(channels.size(), 1, 0);//r0
      //plane.SetSizeZS(1,channels.size(), 0);//ss
      for(size_t i = 0; i < channels.size(); ++i) {
	if(channels[i]){
	  plane.PushPixel(i, 1 , 1);//r0
	  //plane.SetPixel(1, i, 1 , 1); // from Annie		  
	//plane.PushPixel(1, i , 1);//ss
	}
      }

      d2->AddPlane(plane);
			
    /*else{
#if 0
      //Raw
      uint32_t strN = it->second.first;
      uint32_t plane_id = PLANE_ID_OFFSET_ABC + 90 + strN; 
      std::vector<uint8_t> block_data = raw->GetBlock(block_n);
      // But our data is 64bit
      const uint64_t *block = reinterpret_cast<const uint64_t *>(&block_data[0]);
      eudaq::StandardPlane plane(plane_id, "ITS_ABC", "ABC/Raw");
      // X-axis is size, Y-axis is first L0ID
      plane.SetSizeZS(100, 256, 0);

      if(block_data.size() > 8) {
	// Need at least one 64-bit word
	//   (data_64 >> 42) & 0xff;
	unsigned int l0id = (block[1] >> 41) & 0xff;

	plane.PushPixel(block_data.size()/8, l0id, 1);
      } else {
	plane.PushPixel(block_data.size()/8, 0, 1);
      }
      d2->AddPlane(plane);
#endif
    }*/
  }
  //std::cout << std::endl;
  return true;
}
