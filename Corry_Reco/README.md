## Instructions and commands to perform the reconstruction of Test Beam  Data with Corryvreckan

The following is an example on how to use Corryvreckan. Its experimental setup is:
- Six *Mimosa26* planes with a DUT (*ATLASpix*);
- additional timing reference plane (*Timepix3*).

First, you need to copy the folder containing the data, configuration and geometry files needed to run the examples. The needed files can be copied inside Corry main folder or in a separate one. 
To copy the folder, do: 
```bash 
cp -r /nfs/dust/atlas/user/abiondin/QualTask/examples_corryvreckan ${destination_path}
```
To run the analysis, go to the folder were you have the Corry executable and then type:
```bash
corry -c ${PATH_TO_examples_corryvreckan}/03_desy/02_tlu_mimosa26_timepix3_atlaspix.conf 
```
Event loop is udpated continuosly and info are given on screen while running, all the reconstruction is executed in one step.

The output root file will be written in `${PATH_WHERE_YOU_RUN}/output/`. 
This root file contains histograms of all the modules used.

If you want to quit the reconstruction while running, 2 options:
1. `Ctrl+C`: finishes current events and finalize properly (gently quit);
1. `Ctrl+\`:force quit, loss of all generated ouptut data (hard quit).

## Run on April 2019 DESY TestBeam
Run on April 2019 DESY TestBeam with the following experimental setup:
- Six 	`Mimosa26` planes with a DUT (ATLAS ITk Long Strip Module) and fei4.

Informations about the Run Conditions, experimental setup, ecc, can be found at 
[RunLog]( https://docs.google.com/document/d/1VYdjTf2Fg_Tgb6w2_Mc8il2cmgYTLvXQyHOWehGTCdo/edit#heading=h.ulx4ieg4rmwp ) and 
[RunList]( https://docs.google.com/spreadsheets/d/170H_cF9ZRMoRLYfPbfIOK25BEHUqqlOT8HJna0eVbUE/edit#gid=1617373211 ).

The Run we will use as benchmark (*run000474*) can be accessed at `/nfs/dust/atlas/user/abiondin/QualTask/Raw_Data/` .

The configuration and geometry files needed can be found inside the Corry_Reco folder of the repository.

To run the analysis, go to the folder were you have the corry executable and then type:
```bash
corry -c ${PATH_TO_CONFIG_FILE} 
```
The output root file will be written in `${PATH_WHERE_YOU_RUN}/output/`.
This root file contains histograms of all the modules used and in his current version will also suggest cuts to improve the alignment of the planes as this Run's reconstruction is still WIP.
If you want to quit the reconstruction while running, 2 options:
1. `Ctrl+C`: finishes current events and finalize properly (gently quit)
2. `Ctrl+\`: force quit, loss of all generated ouptut data (hard quit)