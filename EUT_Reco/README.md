## Instructions and commands to perform the whole EUTelescope reconstruction workflow on ITk Barrel strip Test Beam Data
The experimental setup for the example we are going to run is: 
- Six `Mimosa26` planes with a DUT (ATLAS ITk Strip sensor);
- additional timing reference plane (ATLAS FEI4 pixel sensor).

Data from DURANTA telescope at 4 GeV/c recorded with EUDAQ2. Use General Broken Line (GBL) algorithm for alignment and fitting.


Once we have our EUT environment setup we can run a step-by-step reconstruction, by doing:
```bash
cd jobsub/examples/GBL_DUT
jobsub -c config.cfg -csv runlist.csv -g noisypixel 2365
jobsub -c config.cfg -csv runlist.csv -g clustering 2365
jobsub -c config.cfg -csv runlist.csv -g hitmaker 2365
jobsub -c config.cfg -csv runlist.csv -g alignGBL1 2365
jobsub -c config.cfg -csv runlist.csv -g alignGBL2 2365
jobsub -c config.cfg -csv runlist.csv -g alignGBL3 2365
jobsub -c config.cfg -csv runlist.csv -g fitGBL 2365
```
In the `config.cfg` file you can change a number of parameters, for example decide the number of events to process. The suggestion is to first go through the whole chain running on a fraction of the total N of events (30k for instance).
After each of the alignGBL steps, use the cuts suggestion that pops out on the screen to optimize your alignment.
Then run the fullreconstruction with all events available, doing: 
```bash
jobsub -c config.cfg -csv runlist.csv -g fullreconstruction 2365
```
The output file is a root file called `Ntuple.root`, located in `jobsub/examples/GBL_DUT`. You can look at histograms after each step in `jobsub/examples/GBL_DUT/output/histograms`.

In the root files the planes are identified by a number, that is usually different depending on the single run of data. In this case DUT (ITk barrel strip) is 11 and ATLAS FEI4 pixel sensor is 7.