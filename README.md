This README si divided into sections. Each one of those includes the instructions to install one framework. 

NOTE: all setups and installation steps are tested to work for Centos7 NAF machines  


## Setting up EUDAQ2
EUDAQ2 repository is a submodule of the main repository. 
```bash
mkdir ${your_working_dir}
git clone ssh://git@gitlab.cern.ch:7999/abiondin/qualificationtask.git
cd qualificationtask
source setup.sh
git submodule update --init --recursive
```
This last command will clone the main repository for all submodules (EUDAQ2 and Corryvreckan).
```bash
cd eudaq
git tag
git checkout v2.4.4 
cd ..
```
```

##The Converter for ITk strips must be added manually; 
later this will be included in the default EUDAQ installation.
The converter file is in the folder `Corry_Reco` of the repository and it needs to be copied and put in `eudaq/user/itkstrip/module/src/` using:
```bash
cp Corry_Reco/ItsAbcRawEvent2StdEventConverter.cc eudaq/user/itkstrip/module/src/.
```

We can then proceed with the EUDAQ installation.


## Installation 
```bash
cd eudaq
mkdir build
cd build

(NAF Centos7 machine) cmake -DUSER_TIMEPIX3_BUILD=ON -DUSER_STCONTROL_BUILD=ON -DUSER_ITKSTRIP_BUILD=ON -DEUDAQ_LIBRARY_BUIL\
D_LCIO=ON -DEUDAQ_LIBRARY_BUILD_TTREE=ON -DEUDAQ_BUILD_STDEVENT_MONITOR=ON -DEUDAQ_BUILD_GUI=ON -DUSER_TLU_BUILD_EUDET=ON -D\
USER_BUILD_TLU_ONLY_CONVERTER=ON ..
(lxplus) cmake -DUSER_TIMEPIX3_BUILD=ON -DUSER_STCONTROL_BUILD=ON -DUSER_ITKSTRIP_BUILD=ON -DEUDAQ_LIBRARY_BUILD_LCIO=ON -DE\
UDAQ_LIBRARY_BUILD_TTREE=ON -DEUDAQ_BUILD_STDEVENT_MONITOR=ON -DEUDAQ_BUILD_GUI=ON -DUSER_TLU_BUILD_EUDET=ON -DUSER_TLU_BUIL\
D=ON ..
make install -j{#core}
```
If not specified, all the listed modules are not built in the EUDAQ2 default installation.
To link correctly the EUDAQ2 installation to Corryvreckan, you need to pass the installation path to CMake when building Corryvreckan via:
```
cmake -Deudaq_DIR=/path/to/eudaq2_installation/cmake/
```
## Setting up Corryvreckan
in the `qualificationtask` folder, do:
```bash
source setup.sh
cd corryvreckan
mkdir build
cd build
```
N.B: Needed modules have to be turned on as most of them are set *OFF* in the default installation.
```bash
cmake -DCMAKE_INSTALL_PREFIX=../install/
      -DCMAKE_BUILD_TYPE=DEBUG
      -Deudaq_DIR=/path/to/eudaq2_installation/cmake/
      -DBUILD_EventLoaderEUDAQ2=ON ..
make install -j{#cores}
```
The binary is now available as `bin/corry` in the installation directory.

## Setting up Eutelescope
EUTelescope version for ITk strip Barrel data reconstruction needs a non trivial effort for installation, as it includes dependencies and installation of a large variety of frameworks.
Therefore if you have access to NAF AFS you can copy the jobsub folder from my EUTelescope installation, doing: 
```bash
cp -r /nfs/dust/atlas/user/abiondin/QualTask/BARREL/eutel_v02-00-00/Eutelescope/master/jobsub ${destination_path}
```
The EUTelescope environment can be directly sourced doing:
```bash
source /nfs/dust/atlas/user/abiondin/QualTask/BARREL/eutel_v02-00-00/Eutelescope/master/build_env.sh
```
*CAVEAT:* since you have changed now the location of the jobsub and examples, you have to tell EUTelescope where you want to execute the reconstruction. To do that, one needs to change the `BasePath` in the configuration file with your own path.
