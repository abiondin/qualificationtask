#This setup.sh setup the environment for EUDAQ2 and Corryvreckan
#setup tested and working for Centos7 NAF machines

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.16.00-x86_64-slc6-gcc62-opt"
#lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "cmake 3.11.0"
lsetup "gcc gcc620_x86_64_slc6"
